<?php

namespace mywishlist\controller;

use mywishlist\model\Item;
use mywishlist\model\Liste;
use mywishlist\view\ItemView;
use mywishlist\view\FormItem;

require_once 'vendor/autoload.php';

class ItemControlleur
{
  public function listItem() {
    $no = $_GET["num"];
    $liste = Liste::where('token','=',$no)->first();
    $items = $liste->hasMany('mywishlist\model\Item','liste_id')->get();
    $v = new ItemView($liste,$items, ITEMS_VIEW);
    $v->render();
  }

  public function listItemExemple($no) {
    $liste = Liste::where('no','=',$no)->first();
    $items = $liste->hasMany('mywishlist\model\Item','liste_id')->get();
    $v = new ItemView($liste,$items, ITEMS_VIEW);
    $v->render();
  }

  public function vueCreationItems(){
    $v = new FormItem();
    $v->render();
  }

  public function creerItem($id){
      $u = new Item;
      $u->liste_id=$id;
      $u->nom=$_POST['nom'];
      $u->descr=$_POST['descr'];
      $u->img=$_POST['urlimg'];
      $u->url=$_POST['url'];
      $u->tarif=$_POST['tarif'];

      $u->save();
    }
/*  public function getItem( $id ) {
    $i = Item::where('id','=',$id);
    $v = new ItemView( $i , ITEM_VIEW ) ;
    $v->render() ;
  }*/
}
