<?php

namespace mywishlist\controller;

use mywishlist\model\Reservation;
use mywishlist\model\Item;
use mywishlist\view\FormResa;
use mywishlist\view\ResaView;

require_once 'vendor/autoload.php';

class ResaController
{
  public function vueCreationReservation($id)
  {
    $v = new FormResa($id);
    $v->render();
  }

  public function afficherReservation($id)
  {
    $v = new ResaView($id);
    $v->render();
  }

  public function creerResa($id){
      $r = new Reservation;
      $r->item_id=$id;
      $r->pseudo=$_POST['pseudo'];
      $r->email=$_POST['email'];
      $r->commentaire=$_POST['comm'];

      $i = Item::where('id','=',$id)->first();
      $i->reservation=1;
      $i->save();

      $r->save();
    }
}
