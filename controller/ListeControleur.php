<?php

namespace mywishlist\controller;

use mywishlist\model\Item;
use mywishlist\model\Liste;
use mywishlist\view\ListeView;
use mywishlist\view\FormListe;

require_once 'vendor/autoload.php';

class ListeControleur
{
  public function afficherListes()
  {
    $listes = Liste::get();
    $v = new ListeView($listes, LISTS_VIEW);
    $v->render();
  }

  public function afficheMessage($msg)
  {
    $v = new ListeView($msg, ERREUR);
    $v->render();
  }


  public function creerListe(){
      $u = new Liste;
      $u->user_id=$_POST['id'];
      $u->titre=$_POST['titre'];
      $u->description=$_POST['descr'];
      $u->expiration=$_POST['expi'];

      $u->save();

      $id = $u->no;

      $u->token = sha1("GET=$id");
      $u->save();
      setcookie("numliste","$id");
    }


    public function vueCreationListe(){
      $v=new FormListe();
      $v->render();
    }
}
