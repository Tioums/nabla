<?php

namespace mywishlist\controller;

use mywishlist\view\VueGeneral;

require_once 'vendor/autoload.php';

class ControlleurGeneral
{
  public function accueil()
  {
    $msg = "Bienvenue sur le site My Wishlist";
    $v = new VueGeneral($msg);
    $v->render();
  }
}
