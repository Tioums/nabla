<?php
namespace mywishlist\controller;

use mywishlist\model\User as User;
use mywishlist\view\FormInscription;
use mywishlist\view\FormConnexion;
use mywishlist\view\VueGeneral;

require_once 'vendor/autoload.php' ;

class UserController
{
  public function inscription()
  {
    $u = new User;
    $u->identifiant=$_POST['id'];
    $u->mdp=$_POST['mdp1'];
    $u->nom=$_POST['nom'];
    $u->prenom=$_POST['prenom'];
    $u->mail=$_POST['mail'];
    $u->tel=$_POST['telephone'];
    $u->adresse=$_POST['adresse'];
    $u->cp=$_POST['cp'];
    $u->ville=$_POST['ville'];

    $u->save();
  }


  public function vueInscription()
  {
    $v=new FormInscription();
    $v->render();
  }

  public function connexion()
  {
    $v=new FormConnexion();
    $v->render();
  }

  public function recupIdMdp() {

      $mailUser = User::select('prenom','mail','mdp')->where('mail','=',$_POST['mail'])->where('mdp','=',$_POST['mdp'])->first();

    	if ($mailUser['mail'] == $_POST['mail'] && $mailUser['mdp'] == $_POST['mdp']) {
    		$_SESSION['ID'] = $mailUser['prenom'];
    		$_SESSION['MDP'] = $_POST['mdp'];
    	}
    	else {
    	  echo '<body onLoad="alert(\'Membre non reconnu...\')">';
    		echo '<meta http-equiv="refresh" content="0;URL=./connexion">';
    	}
  }


}
