<?php

namespace mywishlist\model ;

class User extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'client';
  protected $primaryKey = 'identifiant';
  public $timestamps = false;

}
