<?php

namespace mywishlist\model ;

class Reservation extends \Illuminate\Database\Eloquent\Model {
  protected $table = 'reservation';
  protected $primaryKey = 'resa_id';
  public $timestamps = false;
}
