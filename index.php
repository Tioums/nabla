<?php

require_once 'vendor/autoload.php';

use mywishlist\model\Item;
use mywishlist\model\Liste;
use mywishlist\model\User;
use mywishlist\model\Reservation;
use mywishlist\controller\ListeControleur as ListeControleur;
use mywishlist\controller\UserController as UserController;
use mywishlist\controller\ControlleurGeneral as ControlleurGeneral;
use mywishlist\controller\ItemControlleur as ItemControlleur;
use mywishlist\controller\ResaController as ResaController;

include 'bdd.php';

$app = new Slim\Slim();

$app->get('/hello/:name', function ($name) {
    echo "Hello, " . $name;
});


$app->get('/', function (){
  $accueil = new ControlleurGeneral();
  $accueil->accueil();
})->name('accueil');

$app->get('/afficherListes', function (){
  $listes = new ListeControleur();
  $listes->afficherListes();
});

$app->get('/afficheListe', function (){
  $liste = new ListeControleur();
  $liste->afficherListeChoisie();
});

$app->get('/afficheItems', function (){
  $items = new ItemControlleur();
  $items->listItem();
});

$app->get('/exemple', function (){
  $items = new ItemControlleur();
  $items->listItemExemple(2);
});


$app->get('/inscription', function () {
  $user = new UserController();
  $user->vueInscription();
});

$app->post('/inscription1', function() {
  $user = new UserController();
  $user->inscription();
  $user->connexion();
})->name('inscription1');


$app->get('/connexion', function () {
  $user = new UserController();
  $user->connexion();
});

$app->post('/connexion1', function () {
  $user = new UserController();
  $user->recupIdMdp();

  $accueil = new ControlleurGeneral();
  $accueil->accueil();
});


$app->get('/creerliste', function () {
  $liste = new ListeControleur();
  if (!isset($_COOKIE["numliste"])) {
  $liste->vueCreationListe();
}
else{
  $msg = "oops, ceci n'est pas possible..."."<br /><br />Vous avez déjà une liste en cours !";
  $liste->afficheMessage($msg);
}
});


$app->post('/listecreee', function() {
  $liste = new ListeControleur();
  $item = new ItemControlleur();
  $liste->creerListe();
  $item->vueCreationItems();
})->name('listecreee');

$app->get('/creeritem', function () {
  if (isset($_COOKIE["numliste"])) {
    $item = new ItemControlleur();
    $item->vueCreationItems();
  }
  else{
    $liste = new ListeControleur();
    $msg = "oops, ceci n'est pas possible..."."<br /><br />Vous devez être connecté(e) et/ou avoir créé(e) une liste pour accéder à cette partie du site !";
    $liste->afficheMessage($msg);
  }
});

$app->post('/reservationok', function () {
  if (isset($_COOKIE["item_id"])) {
    $resa = new ResaController();
    $resa->creerResa($_COOKIE["item_id"]);

    $i = Item::where('id','=',$_COOKIE["item_id"])->first();
    $l = $i->belongsTo('mywishlist\model\Liste','liste_id')->first();
    $items = new ItemControlleur();
    $items->listItemExemple($l->no);
    ?>
    <script>
    $msg = "Réservation effectuée avec succès";
    alert($msg);
    </script>
    <?php
  }
  else{
    $liste = new ListeControleur();
    $msg = "oops, ceci n'est pas possible..."."<br /><br />Vous devez être connecté(e) et/ou avoir créé(e) une liste pour accéder à cette partie du site !";
    $liste->afficheMessage($msg);
  }
})->name('resaok');


$app->post('/itemcree', function() {
  $item = new ItemControlleur();
  $item->creerItem($_COOKIE["numliste"]);
  $item->vueCreationItems();
})->name('itemcree');

$app->get('/reservation', function() {
  $resa = new ResaController();
  $resa->vueCreationReservation($_COOKIE["item_id"]);
})->name('reservation');

$app->get('/reservation1', function() {
  $resa = new ResaController();
  $resa->afficherReservation($_COOKIE["item_id"]);
})->name('reservation1');


$app->get('/affichelistecreee', function() {
  $liste= new ListeControleur();
  $items = new ItemControlleur();
  if (isset($_COOKIE["numliste"])) {
    $items->listItemExemple($_COOKIE["numliste"]);
  }
  else{
    $msg = "oops, ceci n'est pas possible..."."<br /><br />Vous devez être connecté(e) et/ou avoir créé(e) une liste pour accéder à cette partie du site !";
    $liste->afficheMessage($msg);
  }
})->name('affichelistecreee');

$app->run();
