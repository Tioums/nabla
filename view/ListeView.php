<?php

namespace mywishlist\view;



class ListeView{

  protected $liste, $selecteur;

  public function __construct($i,$s){
    $this->liste = $i;
    $this->selecteur = $s;
  }

  public function multiHtmlTitre(){
    foreach ($this->liste as $key) {
    $content = $content."<li>$key->titre</li>";
    }
    return $content;
  }


  public function soloHtmlTitre(){
    $titre = $this->liste->titre;
    $content = "<li>$titre</li>";
    return $content;
  }

  public function soloHtml(){
    $titre = $this->liste;
    $content = "<h1>$titre</h1>";
    return $content;
  }

  public function soloHtmlUrl(){
    $titre = $this->liste;
    $content = "<h1>$titre</h1>";
    return $content;
  }

  public function afficheMenu(){
    session_start();
    if (isset($_SESSION['ID']) && isset($_SESSION['MDP']))
    {
      $menu = "	<div class='wifeo_rubrique'>
      <a>Compte</a>
      <div class='wifeo_sousmenu'>
      <div class='wifeo_pagesousmenu'>
      <a>Bonjour ".$_SESSION['ID']."</a>
      </div>
      <div class='wifeo_pagesousmenu'>
      <a href='./view/deconnexion.php'>Déconnexion</a>
      </div>
      </div>
      </div>";
    }else
    {
      $menu = "	<div class='wifeo_rubrique'>
      <a href='./connexion'>Identifiez-vous</a>
      <div class='wifeo_sousmenu'>
      <div class='wifeo_pagesousmenu'>
      <a href='./connexion'>Connectez-vous</a>
      </div>
      <div class='wifeo_pagesousmenu'>
      <a href='./inscription'>Nouveau ? <br>Inscrivez-vous</a>
      </div>
      </div>
      </div>";
    };
    return $menu;
  }



  public function render(){

    switch ($this->selecteur) {
 case LISTS_VIEW : {
 $content = $this->multiHtmlTitre();
 break;
 }
 case LIST_VIEW : {
 $content = $this->soloHtmlTitre();
 break;
 }
 case ERREUR : {
 $content = $this->soloHtml();
 break;
 }
}

  $menu = $this->afficheMenu();
  
    $html = <<<END
<!DOCTYPE html">
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="../view/css/Beldier.css">
	<link rel="stylesheet" type="text/css" href="../view/css/Index.css">
	<link rel="stylesheet" type="text/css" href="../view/css/Menu.css">
    <title>My Wishlist - Réalisez vos rêves !</title>
    </head>

  <body>
  <div id="conteneur">
    <h1 id="header"><a href="./" title="My Wishlist - Accueil"><span>Wishlist</span></a></h1>

    <nav>
      <div class="wifeo_conteneur_menu">

        <div class="wifeo_pagemenu">
          <a href="./">Accueil</a>
        </div>

        <div class="wifeo_pagemenu">
          <a href="./affichelistecreee">Ma Liste</a>
        </div>

        <div class="wifeo_pagemenu">
          <a href="./creeritem"> Ajouter un Objet </a>
        </div>

        <div class="wifeo_pagemenu">
          <a href="./creerliste">Créer une liste</a>
        </div>

        $menu

        <div class="wifeo_pagemenu">
          <a href="./exemple"> Exemple </a>
        </div>


      </div>
    </nav>

    <div id="contenu">
      <br />
      <ul>
          $content
        <br />
      </ul>
      <br />
    </div>
    <p id="footer">R&eacute;alis&eacute;s par Gaetan Lagraviere, Nicolas Lardier et Virgil Sadon - &copy; 2017-2018.</p>
  </div>
  </body>
</html>
END;
    echo $html;

  }
}
