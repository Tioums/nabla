<?php

namespace mywishlist\view;

class FormItem{

public function render(){
$app = \Slim\Slim::getInstance();
$url = $app->urlFor('itemcree');//Router vers ajouter des Items

$html = <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Creation d'objet</title>
  <link rel="stylesheet" type="text/css" href="../view/css/inscription.css">
</head>
<BODY>

  <div id="signup-form">
    <div id="signup-inner">

      <div class="clearfix" id="header">
        <img id="signup-icon" src="./images_Inscription/signup.png" alt="" />
        <h1>Ajouter un objet à votre liste</h1>
      </div>

      <p>Compl&eacutetez les champs ci-dessous.</p>

      <form id="send" action=$url method="POST"  name="formulaire"  onSubmit="return check();">
        <p>
          <label for="nom">nom de l'objet *</label>
          <input id="nom" type="text" name="nom" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="descr">Description de l'objet</label>
          <input id="descr" type="text" name="descr" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="urlimg">Url de l'image de l'objet</label>
          <input id="urlimg" type="url" name="urlimg" value="" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="url">Url de l'objet à acheter</label>
          <input id="url" type="url" name="url" value="" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="tarif">Prix de l'objet</label>
          <input id="tarif" type="number" name="tarif" value="" onKeyUp="javascript:couleur(this);"/>
        </p>

        <div id="requis">
          <p>* Champs Requis<br/></p>
        </div>

        <p>
          <button id="submit" type="submit" name="Submit">Envoyer</button>
          <button id="submit" type="reset">Reinitialiser</button>
          <button id="submit" type="button" onclick="location.href='../index.php/'" >Accueil</button>
        </p>

      </form>
    </div>

  </div>

  <script src="mywishlist/view/js/inscription.js"></script>

</body>
</html>

END;
echo $html;
}

}
