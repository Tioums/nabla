<?php

namespace mywishlist\view;



class VueGeneral{

  protected $liste;

  public function __construct($i){
    $this->liste = $i;
  }


  public function soloHtml(){
    $content = "<h1>$this->liste</h1>";
    return $content;
  }

  public function afficheMenu(){
    session_start();
    if (isset($_SESSION['ID']) && isset($_SESSION['MDP']))
    {
      $menu = "	<div class='wifeo_rubrique'>
      <a>Compte</a>
      <div class='wifeo_sousmenu'>
      <div class='wifeo_pagesousmenu'>
      <a>Bonjour ".$_SESSION['ID']."</a>
      </div>
      <div class='wifeo_pagesousmenu'>
      <a href='./view/deconnexion.php'>Déconnexion</a>
      </div>
      </div>
      </div>";
    }else
    {
      $menu = "	<div class='wifeo_rubrique'>
      <a href='./connexion'>Identifiez-vous</a>
      <div class='wifeo_sousmenu'>
      <div class='wifeo_pagesousmenu'>
      <a href='./connexion'>Connectez-vous</a>
      </div>
      <div class='wifeo_pagesousmenu'>
      <a href='./inscription'>Nouveau ? <br>Inscrivez-vous</a>
      </div>
      </div>
      </div>";
    };
    return $menu;
  }


  public function render(){

      $content = $this->soloHtml();
      $menu = $this->afficheMenu();

      $html = <<<END
  <!DOCTYPE html">
  <html lang="fr">
    <head>
      <meta charset="utf-8" />
      <link rel="stylesheet" type="text/css" href="../view/css/Beldier.css">
    <link rel="stylesheet" type="text/css" href="../view/css/Index.css">
    <link rel="stylesheet" type="text/css" href="../view/css/Menu.css">
      <title>My Wishlist - Réalisez vos rêves !</title>
      </head>

    <body>
    <div id="conteneur">
      <h1 id="header"><a href="./" title="My Wishlist - Accueil"><span>Wishlist</span></a></h1>

      <nav>
        <div class="wifeo_conteneur_menu">

          <div class="wifeo_pagemenu">
            <a href="./">Accueil</a>
          </div>

          <div class="wifeo_pagemenu">
            <a href="./affichelistecreee">Ma Liste</a>
          </div>

          <div class="wifeo_pagemenu">
            <a href="./creeritem"> Ajouter un Objet </a>
          </div>

          <div class="wifeo_pagemenu">
            <a href="./creerliste">Créer une liste</a>
          </div>

          $menu

          <div class="wifeo_pagemenu">
            <a href="./exemple"> Exemple </a>
          </div>



        </div>
      </nav>

      <div id="contenu">
        <br />
            $content
          <br />
        <br />
        <h2>Toutes vos envies dans une seule liste !</h2>
        <br />
        <p>Pour offrir à votre enfant les cadeaux qui lui feront plaisir
        à Noël, pour son anniversaire ou pour toutes autres occasions nous vous proposons de créer votre liste de souhaits.<br />
        Très simple à réaliser, vous pouvez la partager avec vos proches pour les guider dans leurs achats !</p>

        <h2>Comment ça marche ?</h2>
        <ul>
        <li>Je me connecte ou je créé mon compte si ce n'est pas encore fait.</li>
        <li>Je commence la création de ma liste en cliquant sur "Créer une liste".</li>
        <li>J'obtiens un lien de partage unique pour l'envoyer plus tard à mes proches.</li>
        <li>J'ajoute les objets que je désire à l'intérieur.</li>
        </ul>
      </div>
      <p id="footer">R&eacute;alis&eacute;s par Gaetan Lagraviere, Nicolas Lardier et Virgil Sadon - &copy; 2017-2018.</p>
    </div>
    </body>
  </html>
END;
      echo $html;

    }
  }
