<?php

namespace mywishlist\view;

class FormListe{

public function render(){
$app = \Slim\Slim::getInstance();
$url = $app->urlFor('listecreee');//Router vers ajouter des Items

$html = <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Creation de liste</title>
  <link rel="stylesheet" type="text/css" href="../view/css/inscription.css">
</head>
<BODY>

  <div id="signup-form">
    <div id="signup-inner">

      <div class="clearfix" id="header">
        <img id="signup-icon" src="./images_Inscription/signup.png" alt="" />
        <h1>Créer une nouvelle liste</h1>
      </div>

      <p>Compl&eacutetez les champs ci-dessous.</p>

      <form id="send" action=$url method="POST"  name="formulaire"  onSubmit="return check();">
        <p>
          <label for="id">Pseudo</label>
          <input id="id" type="text" name="id" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="titre">Titre *</label>
          <input id="titre" type="text" name="titre" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="descr">Description *</label>
          <input id="descr" type="text" name="descr" value="" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="expi">Date d'expiration voulue *</label>
          <input id="expi" type="date" name="expi" value="" onKeyUp="javascript:couleur(this);"/>
        </p>

        <div id="requis">
          <p>* Champs Requis<br/></p>
        </div>

        <p>
          <button id="submit" type="submit" name="Submit">Envoyer</button>
          <button id="submit" type="reset">Reinitialiser</button>
          <button id="submit" type="button" onclick="location.href='../index.php/'" >Accueil</button>
        </p>

      </form>
    </div>

  </div>

  <script src="mywishlist/view/js/inscription.js"></script>

</body>
</html>


END;
echo $html;
}

}
