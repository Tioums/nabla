<?php

namespace mywishlist\view;



class ItemView{

  protected $liste, $items, $selecteur;

  public function __construct($l,$i,$s){
    $this->liste = $l;
    $this->items = $i;
    $this->selecteur = $s;
  }

  public function multiHtml(){
    $app = \Slim\Slim::getInstance();
    $url = $app->urlFor('reservation');
    $url1 = $app->urlFor('reservation1');

    foreach ($this->items as $key) {
    $content = $content."<TR>";
    if ($key->img == null) {
      $content = $content."<TD><div class=\"center\"><img src=\"https://images.emojiterra.com/mozilla/512px/2753.png\"/></TD>";
    }
    else{
      $content = $content."<TD><div class=\"center\"><img src=\"$key->img\"/></TD>";
    }
    $content = $content."<TD><div class=\"nom\">$key->nom</TD>".
    "<TD><div class=\"descr\">$key->descr";
    if ($key->url!=null) {
      $content = $content."<br /><br /><a href=$key->url target=\"_blank\">Visiter la page du magasin</a>";
    }
    $content = $content."</TD><TD><div class=\"tarif\">$key->tarif</TD>";
    if ($key->reservation == 0) {
        $content = $content."<TD><div class=\"reserv\"><a href=$url onclick=\"SetCookie('item_id','$key->id','1')\"><img src=\"../view/Images/croixrouge.png\"/></TD>";
    }
    else{
      $content = $content."<TD><div class=\"reserv\"><a href=$url1 onclick=\"SetCookie('item_id','$key->id','1')\"><img src=\"../view/Images/cehck.png\"/></TD>";
    }
    $content = $content."</TR>";
    }
    return $content;
  }

  public function soloHtml(){
    $content = $this->liste;
    return $content;
  }


  public function soloHtmlUrl(){
    $token = $this->liste->token;
    $content = "Le lien de partage de votre liste est : <strong>https://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']."/afficheItems?num="."$token</strong>";
    return $content;
  }

  public function soloHtmlTitre(){
    $content = $this->liste->titre;
    return $content;
  }

  public function soloHtmlExp(){
    $content = $this->liste->expiration;
    return $content;
  }

  public function soloHtmlDescr(){
    $content1 = $this->liste->description;
    return $content1;
  }

  public function render(){
      switch ($this->selecteur) {
        case ITEMS_VIEW : {
          $content4 = $this->soloHtmlUrl();
          $content3 = $this->soloHtmlExp();
          $content2 = $this->soloHtmlDescr();
          $content1 = $this->soloHtmlTitre();
          $content = $this->multiHtml();
        break;
        }
        case ITEM_VIEW : {
          $content = $this->soloHtml();
        break;
        }
      }

      $html = <<<END
  <!DOCTYPE html">
  <html lang="fr">
    <head>
      <meta charset="utf-8" />
      <link rel="stylesheet" type="text/css" href="../view/css/Beldier.css">
    <link rel="stylesheet" type="text/css" href="../view/css/Index.css">
    <link rel="stylesheet" type="text/css" href="../view/css/Menu.css">
    <link rel="stylesheet" type="text/css" href="../view/css/Autres.css">
      <title>My Wishlist - Réalisez vos rêves !</title>
      </head>

    <body>
    <div id="conteneur">
      <h1 id="header"><a href="./" title="My Wishlist - Accueil"><span>Wishlist</span></a></h1>

      <nav>
        <div class="wifeo_conteneur_menu">

          <div class="wifeo_pagemenu">
            <a href="./">Accueil</a>
          </div>

          <div class="wifeo_pagemenu">
            <a href="./affichelistecreee">Ma Liste</a>
          </div>

          <div class="wifeo_pagemenu">
            <a href="./creeritem"> Ajouter un Objet </a>
          </div>

          <div class="wifeo_pagemenu">
            <a href="./creerliste">Créer une liste</a>
          </div>

          <div class="wifeo_pagemenu">
            <a href="./connexion"> Connexion/inscription </a>
          </div>

          <div class="wifeo_pagemenu">
            <a href="./exemple"> Exemple </a>
          </div>


        </div>
      </nav>

      <div id="contenu">
        <br />
          <h1>$content1</h1>
          <br /><br />
          <p>$content2</p>
          <br />
          $content4
          <br />
        <table>
        <CAPTION></CAPTION>

        <br />
          <TR>
            <TH> Image </TH>
            <TH> Nom </TH>
            <TH> Description </TH>
            <TH> Tarif </TH>
            <TH> Reservation </TH>
          </TR>

            $content


          <br />
        </table>
        <div class="expir"><p>La liste expirera le : $content3</p></div>
        <br />
      </div>
      <p id="footer">R&eacute;alis&eacute;s par Gaetan Lagraviere, Nicolas Lardier et Virgil Sadon - &copy; 2017-2018.</p>
    </div>

    <script>function SetCookie(c_name,value,expiredays)
	{
		var exdate=new Date()
		exdate.setDate(exdate.getDate()+expiredays)
		document.cookie=c_name+ "=" +escape(value)+
		((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
	}
</script>

    </body>
  </html>
END;
      echo $html;

    }
  }
