<?php

namespace mywishlist\view;

class FormInscription{


public function render(){
$app = \Slim\Slim::getInstance();
$url = $app->urlFor('inscription1');

$html = <<<END
<!DOCTYPE html">
<html lang="fr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Inscription sur My Wishlist - Réalisez vos rêves !</title>
  <link rel="stylesheet" type="text/css" href="../view/css/inscription.css">
</head>
<BODY>

  <div id="signup-form">
    <div id="signup-inner">

      <div class="clearfix" id="header">
        <h1>Inscrivez-vous sur mywishlist !</h1>
      </div>

      <p>Compl&eacutetez les champs ci-dessous.</p>

      <form id="send" action=$url method="POST"  name="formulaire" onSubmit="return check();">
        <p>
          <label for="id">Identifiant *</label>
          <input id="id" type="text" name="id" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="mdp">Mot de passe *</label>
          <input id="mdp" type="password" name="mdp1" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="mdp">Confirmer mot de passe *</label>
          <input id="mdp2" type="password" name="mdp2" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="Nom">Nom *</label>
          <input id="Nom" name="nom" type="text" onKeyUp="javascript:couleur(this);"><br>
        </p>

        <p>
          <label for="prenom">Prenom *</label>
          <input id="prenom" name="prenom" type="text" onKeyUp="javascript:couleur(this);" />
        </p>

        <p>
          <label for="Email">E-mail *</label>
          <input id="Email" name="mail" type="text" onKeyUp="javascript:couleur(this);" />
        </p>

        <p>
          <label for="Email">Confirmez votre adresse e-mail *</label>
          <input id="Email2" name="mail2" type="text" onKeyUp="javascript:couleur(this);" />
        </p>

        <p>
          <label for="telephone">Telephone* </label>
          <input id="telephone" type="text" name="telephone" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="Adresse">Adresse *</label>
          <input id="Adresse" type="text" name="adresse" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="Code postale">Code postale *</label>
          <input id="Code postale" type="text" name="cp" onKeyUp="javascript:couleur(this);"/>
        </p>

        <p>
          <label for="ville">Ville *</label>
          <input id="Ville" type="text" name="ville" onKeyUp="javascript:couleur(this);"/>
        </p>

        <div id="requis">
          <p>* Champs Requis<br/></p>
        </div>

        <p>
          <button id="submit" type="submit" name="Submit">Envoyer</button>
          <button id="submit2" type="reset">Reinitialiser</button>
          <button id="submit3" type="button" onclick="location.href='../index.php/'" >Accueil</button>
        </p>

      </form>
    </div>

  </div>

  <script src="./view/js/inscription.js"></script>

</body>
</html>

END;
echo $html;
}

}
