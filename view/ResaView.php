<?php

namespace mywishlist\view;

use mywishlist\model\Item;
use mywishlist\model\Reservation;
use mywishlist\model\Liste;


class ResaView{

  protected $item;

  public function __construct($i){
    $this->item = $i;
  }

  public function afficheMenu(){
    session_start();
    if (isset($_SESSION['ID']) && isset($_SESSION['MDP']))
    {
      $menu = "	<div class='wifeo_rubrique'>
      <a>Compte</a>
      <div class='wifeo_sousmenu'>
      <div class='wifeo_pagesousmenu'>
      <a>Bonjour ".$_SESSION['ID']."</a>
      </div>
      <div class='wifeo_pagesousmenu'>
      <a href='./view/deconnexion.php'>Déconnexion</a>
      </div>
      </div>
      </div>";
    }else
    {
      $menu = "	<div class='wifeo_rubrique'>
      <a href='./connexion'>Identifiez-vous</a>
      <div class='wifeo_sousmenu'>
      <div class='wifeo_pagesousmenu'>
      <a href='./connexion'>Connectez-vous</a>
      </div>
      <div class='wifeo_pagesousmenu'>
      <a href='./inscription'>Nouveau ? <br>Inscrivez-vous</a>
      </div>
      </div>
      </div>";
    };
    return $menu;
  }



  public function render(){
    $obj = Item::where('id','=',$this->item)->first();
    $resa = Reservation::where('item_id','=',$this->item)->first();
    $l = $obj->belongsTo('mywishlist\model\Liste','liste_id')->first();
    $liste = Liste::where('no','=',$l->no)->first();

    $menu = $this->afficheMenu();

    $html = <<<END
    <!DOCTYPE html">
    <html lang="fr">
    <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="../view/css/Beldier.css">
    <link rel="stylesheet" type="text/css" href="../view/css/Index.css">
    <link rel="stylesheet" type="text/css" href="../view/css/Menu.css">
    <title>Informations réservation</title>
    </head>

    <body>
    <div id="conteneur">
    <h1 id="header"><a href="./" title="My Wishlist - Reservation"><span>Wishlist</span></a></h1>

    <nav>
      <div class="wifeo_conteneur_menu">

        <div class="wifeo_pagemenu">
          <a href="./">Accueil</a>
        </div>

        <div class="wifeo_pagemenu">
          <a href="./affichelistecreee">Ma Liste</a>
        </div>

        <div class="wifeo_pagemenu">
          <a href="./creeritem"> Ajouter un Objet </a>
        </div>

        <div class="wifeo_pagemenu">
          <a href="./creerliste">Créer une liste</a>
        </div>

        $menu

        <div class="wifeo_pagemenu">
          <a href="./exemple"> Exemple </a>
        </div>



      </div>
    </nav>

    <div id="contenu">
      <br />
          <h1>L'objet $obj->nom est bien réservé</h1>
      <br />
          <h3>Il est composé de $obj->descr</h3>
        <br />
      <br />
      <h2>L'objet a été réservé par : $resa->pseudo</h2>
      <br />
      <p>Il t'a laissé un commentaire : </p>
      <p>$resa->commentaire</p>
      <br />
      <p>Tu peux le remercier par email en cliquant <a href="$resa->email"> ici </a></p>
      <br />

        <button id="submit" type="button" onclick="location.href='../index.php/afficheItems?num=$liste->token'" >Retour à la liste</button>
      </div>
    </div>
    <p id="footer">R&eacute;alis&eacute;s par Gaetan Lagraviere, Nicolas Lardier et Virgil Sadon - &copy; 2017-2018.</p>
    </div>
    </body>
    </html>
END;
    echo $html;

    }
    }
