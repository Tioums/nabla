<?php

namespace mywishlist\view;

class FormConnexion{

  public function afficheMenu(){
    if (isset($_SESSION['ID']) && isset($_SESSION['MDP']))
    {
      $menu = "	<div class='wifeo_rubrique'>
      <a>Compte</a>
      <div class='wifeo_sousmenu'>
      <div class='wifeo_pagesousmenu'>
      <a>Bonjour ".$_SESSION['ID']."</a>
      </div>
      <div class='wifeo_pagesousmenu'>
      <a href='./view/deconnexion.php'>Déconnexion</a>
      </div>
      </div>
      </div>";
    }else
    {
      $menu = "	<div class='wifeo_rubrique'>
      <a href='./connexion'>Identifiez-vous</a>
      <div class='wifeo_sousmenu'>
      <div class='wifeo_pagesousmenu'>
      <a href='./connexion'>Connectez-vous</a>
      </div>
      <div class='wifeo_pagesousmenu'>
      <a href='./inscription'>Nouveau ? <br>Inscrivez-vous</a>
      </div>
      </div>
      </div>";
    };
    return $menu;
  }

public function render(){
  $menu = $this->afficheMenu();
$html = <<<END

<!DOCTYPE html">
<html lang="fr">
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" type="text/css" href="../view/css/Beldier.css">
  <link rel="stylesheet" type="text/css" href="../view/css/Menu.css">
  <link rel="stylesheet" type="text/css" href="../view/css/Index.css">
  <title>My Wishlist - Connexion</title>
</head>

<body>
  <div id="conteneur">
    <h1 id="header"><a href="./" title="My Wishlist - Connexion"><span>My Wishlist</span></a></h1>

    <nav>
      <div class="wifeo_conteneur_menu">

        <div class="wifeo_pagemenu">
          <a href="./">Accueil</a>
        </div>

        <div class="wifeo_pagemenu">
          <a href="affichelistecreee">Ma liste</a>
        </div>

        <div class="wifeo_pagemenu">
          <a href="./creeritem"> Ajouter un Objet </a>
        </div>

        <div class="wifeo_pagemenu">
          <a href="./creerliste">Créer une liste</a>
        </div>

        $menu

        <div class="wifeo_pagemenu">
          <a href="./exemple"> Exemple </a>
        </div>


      </div>
    </nav>

    <br/>

    <form action="./connexion1" method="POST" name="formCon" onSubmit="return check();">
      <table align=center border=10>
        <tr>
          <td align=center> Identifiez-vous <br><br>
            Adresse mail : <input type="texte" name="mail" id="mail"> <br><br>
            Mot de passe : <input type="password" name="mdp" id="mdp"> <br><br>
            <a href="./connexion1"><input type="submit" value="Connexion" ></a> <br><br>
            <p>Nouveau sur MyWishlist ? </p>
            <a href="inscription" target="_blank"><input type="button" value="Créez votre compte immédiatement"></a>

          </td>
        </tr>
      </table>
    </form>
    <p id="footer">R&eacute;alis&eacute;s par Gaetan Lagraviere, Nicolas Lardier et Virgil Sadon - &copy; 2017-2018.</p>
  </div>

  <script src="./view/js/connexion.js"></script>

</body>
</html>

END;
echo $html;
}
}
