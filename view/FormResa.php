<?php

namespace mywishlist\view;



class FormResa{

  protected $item;

  public function __construct($i){
    $this->item = $i;
  }



  public function render(){
    $app = \Slim\Slim::getInstance();
    $url = $app->urlFor('resaok');


    $html = <<<END
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Reservation d'objet</title>
      <link rel="stylesheet" type="text/css" href="../view/css/inscription.css">
    </head>
    <BODY>

      <div id="signup-form">
        <div id="signup-inner">

          <div class="clearfix" id="header">
            <h1>Confirmer votre réservation</h1>
          </div>

          <p>Compl&eacutetez les champs ci-dessous.</p>

          <form id="send" action=$url method="POST"  name="formulaire"  onSubmit="return check();">
            <p>
              <label for="pseudo">Pseudo</label>
              <input id="pseudo" type="text" name="pseudo" onKeyUp="javascript:couleur(this);"/>
            </p>

            <p>
              <label for="email">Email *</label>
              <input id="email" type="text" name="email" onKeyUp="javascript:couleur(this);"/>
            </p>

            <p>
              <label for="comm">Laissez un commentaire (max 255 caractères)</label>
              <input id="comm" type="text" name="comm" value="" onKeyUp="javascript:couleur(this);" style="height:180px;"/>
            </p>


            <div id="requis">
              <p>* Champs Requis<br/></p>
            </div>

            <p>
              <button id="submit" type="submit" name="Submit">Envoyer</button>
              <button id="submit" type="reset">Reinitialiser</button>
              <button id="submit" type="button" onclick="location.href='../index.php/'" >Accueil</button>
            </p>

          </form>
        </div>

      </div>

      <script src="mywishlist/view/js/inscription.js"></script>

    </body>
    </html>
END;
    echo $html;
    }
}
