function check() {
  var msg = "";

  if (document.formCon.mail.value != "")	{
    indexAroba = document.formCon.mail.value.indexOf('@');
    indexPoint = document.formCon.mail.value.indexOf('.');
    if ((indexAroba < 0) || (indexPoint < 0)){
      //dans le cas ou il manque soit le . soit l'@ on modifie la couleur d'arrière plan du champ mail et définissons un message d'alerte
      msg += "Le format de votre adresse e-mail n'est pas valide. Veuillez corriger avant de réessayer.\n\n";
    }
  }
  //Notre champs mail est vide donc on change la couleur et on défini un autre message d'alerte
  else	{
    msg += "Vous devez saisir une adresse e-mail.\n";
  }

  if (document.formCon.mdp.value == "") {
    msg += "Vous devez entrer un mot de passe.\n";
  }


  if (msg == "") return(true);

  else {
    alert(msg);
    return(false);
  }
}
