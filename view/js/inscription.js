function couleur(obj) {
  obj.style.backgroundColor = "#FFFFFF";
}

function check() {
  var msg = "";
  if (document.formulaire.id.value == "") {
    msg += "Veuillez saisir un identifiant.\n";
    document.formulaire.id.style.backgroundColor = "#F3C200";
  }

  if (document.formulaire.mdp1.value == "") {
    msg += "Vous devez entrer un mot de passe.\n";
    document.formulaire.mdp1.style.backgroundColor = "#F3C200";
  }

  if (document.formulaire.mdp1.value != document.formulaire.mdp2.value || document.formulaire.mdp1.value == "" || document.formulaire.mdp2.value == "") {
    document.formulaire.mdp1.style.backgroundColor = "#F3C200"
    document.formulaire.mdp2.style.backgroundColor = "#F3C200"
    msg += "Veuillez vérifier que les mots de passe correspondent et réessayer.\n";
  }

  if (document.formulaire.nom.value == "") {
    msg += "Vous devez entrer un nom.\n";
    document.formulaire.nom.style.backgroundColor = "#F3C200";
  }

  if (document.formulaire.prenom.value == "") {
    msg += "Vous devez entrer un prenom.\n";
    document.formulaire.prenom.style.backgroundColor = "#F3C200";
  }

  //Vérification du mail s'il n'est pas vide on vérifie le . et @

  if (document.formulaire.mail.value != "")	{
    indexAroba = document.formulaire.mail.value.indexOf('@');
    indexPoint = document.formulaire.mail.value.indexOf('.');
    if ((indexAroba < 0) || (indexPoint < 0))		{
      //dans le cas ou il manque soit le . soit l'@ on modifie la couleur d'arrière plan du champ mail et définissons un message d'alerte
      document.formulaire.mail.style.backgroundColor = "#F3C200";
      msg += "Le format de votre adresse e-mail n'est pas valide. Veuillez corriger avant de réessayer.\n\n";
    }
  }

  //Notre champs mail est vide donc on change la couleur et on défini un autre message d'alerte

  else	{
    document.formulaire.mail.style.backgroundColor = "#F3C200";
    msg += "Vous devez saisir une adresse e-mail.\n";
  }


  if (document.formulaire.mail.value != document.formulaire.mail2.value || document.formulaire.mail.value == "" || document.formulaire.mail2.value == "") {
    document.formulaire.mail.style.backgroundColor = "#F3C200"
    document.formulaire.mail2.style.backgroundColor = "#F3C200"
    msg += "Veuillez vérifier que adresse e-mail correspondent et réessayer.\n";
  }

  if (document.formulaire.telephone.value == "") {
    msg += "Vous devez saisir un numero de telephone.\n";
    document.formulaire.telephone.style.backgroundColor = "#F3C200";
  }

  if (document.formulaire.adresse.value == "") {
    msg += "Vous devez entrer un adresse.\n";
    document.formulaire.adresse.style.backgroundColor = "#F3C200";
  }

  if (document.formulaire.cp.value == "") {
    msg += "Vous devez entrer un code postale.\n";
    document.formulaire.cp.style.backgroundColor = "#F3C200";
  }

  if (document.formulaire.ville.value == "") {
    msg += "Vous devez entrer un ville.\n";
    document.formulaire.ville.style.backgroundColor = "#F3C200";
  }

  if (msg == "") return(true);

  else {
    alert(msg);
    return(false);
  }
}
